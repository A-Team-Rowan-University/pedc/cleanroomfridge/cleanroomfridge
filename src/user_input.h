/** \file */

#ifndef USER_INPUT_H
#define USER_INPUT_H

#include <stdbool.h>
#include "types.h"

/// output signals from the open/close module
typedef struct UserInputOutput
{
    DesiredPosition desiredPosition;
} UserInputOutput;

/**
 *  main function for the user_input module
 * \param   isHotButtonPressed      ///< true if the hot button is being pressed
 * \param   isColdButtonPressed     ///< true if the cold button is being pressed
 * \param   isCloseButtonPressed    ///< true if the close button is being pressed
 * \param   actualPosition          ///< the actual position of the chamber
 * \param   openCloseError          ///< the error signal from the open/close module
 * \param   systemTime              ///< the time that has elapsed since system startup
 * \return  output signals from the open/close module based on the input signals and the internal state
 */
UserInputOutput user_input(bool isHotButtonPressed, bool isColdButtonPressed, bool isCloseButtonPressed, ActualPosition actualPosition,
    OpenCloseError openCloseError,Time systemTime);

#endif
