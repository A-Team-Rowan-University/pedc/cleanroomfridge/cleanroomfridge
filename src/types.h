/**
 * \file
 * provides all typedefs for the enums
 */
#ifndef TYPES_H
#define TYPES_H


#include <stdint.h>

/// current state of the chamber
typedef enum ActualPosition
{
    ACTUAL_POSITION_CLOSED,     ///< at the closed position
    ACTUAL_POSITION_CLOSED_HOT, ///< between closed and hot positions
    ACTUAL_POSITION_HOT,        ///< at the hot position
    ACTUAL_POSITION_HOT_COLD,   ///< between hot and cold
    ACTUAL_POSITION_COLD,       ///< at the cold position
} ActualPosition;

/// desired position of the chamber
typedef enum DesiredPosition
{
    DESIRED_POSITION_HOT,    ///< go to hot position
    DESIRED_POSITION_COLD,   ///< go to cold position
    DESIRED_POSITION_CLOSED, ///< go to closed position
} DesiredPosition;

/// possible states for the hot side of the peltier
typedef enum HotPeltierState
{
    HOT_PELTIER_STATE_OK,               ///< the hot side of the peltier is < 50 C
    HOT_PELTIER_STATE_ERR_MELTING,      ///< the hot side of the peltier is > 50C and in danger of melting
    HOT_PELTIER_STATE_ERR_DISCONNECTED, ///< the hot side of the peltier sensor is disconnected
    HOT_PELTIER_STATE_ERR_SHORTED,      ///< the hot side of the peltier sensor is shorted
} HotPeltierState;

/// possible states for the cold side of the peltier
typedef enum ColdPeltierState
{
    COLD_PELTIER_STATE_OK,                  ///< the cold side of the peltier is between 2 and 10 C (within cold range)
    COLD_PELTIER_STATE_HOT,                 ///< the cold side of the peltier is > 10 C
    COLD_PELTIER_STATE_ERR_FREEZE,          ///< the cold side of the peltier is < 0 C and in danger of freezing
    COLD_PELTIER_STATE_ERR_DISCONNECTED,    ///< the cold side of the peltier sensor is disconnected
    COLD_PELTIER_STATE_ERR_SHORTED,         ///< the cold side of the peltier sensor is shorted
} ColdPeltierState;

/// possible states for the air in the hot chamber
typedef enum HotAirState
{
    HOT_AIR_STATE_OK,               ///< the air in the hot chamber is < 50 C and at a safe temperature
    HOT_AIR_STATE_ERR_MELTING,      ///< the air in the hot chamber is >= 50 C and in danger of melting
    HOT_AIR_STATE_ERR_DISCONNECTED, ///< the air in the hot chamber sensor is disconnected
    HOT_AIR_STATE_ERR_SHORTED,      ///< the air in the hot chamber sensor is shorted
} HotAirState;

/// possible states for the air in the cold chamber
typedef enum ColdAirState
{
    COLD_AIR_STATE_OK,                  ///< the air in the cold chamber is between 2 and 10 C, within cold range
    COLD_AIR_STATE_COLD,                ///< the air in the cold chamber is < 2C and colder than the desired temperature
    COLD_AIR_STATE_HOT,                 ///< the air in the cold chamber is > 10 C, hotter than desired range
    COLD_AIR_STATE_ERR_DISCONNECTED,    ///< the air in the cold chamber sensor is disconnected
    COLD_AIR_STATE_ERR_SHORTED,         ///< the air in the cold chamber sensor is shorted
} ColdAirState;

/// temperature in degrees Celsius
typedef int8_t Temperature;

/// brightness of LED, 0 = 0%, 255 = 100%
typedef  uint8_t LedBrightness;

/// repeating light pattern animation
typedef struct LedLightPattern
{
    uint16_t delay; //< delay between each brightness frame in milliseconds
    uint8_t frame_length; //< the number of frames in the light animation
    LedBrightness *frames; //< pointer to an array of LedBrightness values
} LedLightPattern;

/// the state of the bellow
typedef enum BellowState
{
    BELLOW_STATE_INFLATE,   ///< the bellow is in the inflating
    BELLOW_STATE_HOLD,      ///< the bellow is holding it's pressure, neither inflating of deflating
    BELLOW_STATE_DEFLATE,   ///< the bellow is in the deflating
} BellowState;

/// the state of the bellow
typedef enum OpenCloseError
{
    OPEN_CLOSE_ERROR_OK,                    ///< no errors are detected
    OPEN_CLOSE_ERROR_ERR_FAIL_CHANGE_STATE, ///< the open/close system was unable to change state in desired time
    OPEN_CLOSE_ERROR_ERR_FAIL_HOLD_STATE,   ///< the open/close system was unable to hold its desired state
} OpenCloseError;

/// time in milliseconds
typedef uint32_t Time;

#endif // TYPES_H
