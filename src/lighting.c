/** \file */
#include "lighting.h"

#include <stdint.h>
#include <stdbool.h>

/**
 * \todo implement this function
 * \todo write unit test
 */
LightingOutput lighting(HotPeltierState hotPeltierState, ColdPeltierState coldPeltierState,
    HotAirState hotAirState, ColdAirState coldAirState, bool fanErrorHot, bool fanErrorCold, OpenCloseError openCloseError,
    ActualPosition actualPosition, DesiredPosition desiredPosition, Time systemTime)
{
}
