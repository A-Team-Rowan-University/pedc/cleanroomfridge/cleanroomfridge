/** \file */

#include "hardware_out.h"

/**
 * \todo implement this function
 * \todo write unit test
 */
void hardware_out(LedLightPattern ledRing0, LedLightPattern ledRing1, LedLightPattern ledRing2,
    LedLightPattern ledRing3, LedLightPattern ledRing4, LedLightPattern buttonLedHot, LedLightPattern buttonLedCold,
    LedLightPattern buttonLedClose, bool chamberLedHotEnable, bool chamberLedColdEnable, bool hotFanEnable, bool coldFanEnable,
    bool peltierEnable, BellowState bellowState)
{
}
