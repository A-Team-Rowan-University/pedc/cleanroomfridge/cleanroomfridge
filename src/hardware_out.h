/** \file */

#ifndef HARDWARE_OUT_H
#define HARDWARE_OUT_H

#include <stdint.h>
#include <stdbool.h>
#include "types.h"

/**
 * sets the hardware output based on submodule inputs
 * \param ledRing0              the LedLightPattern for ring 0 (bottom) in the outer shell
 * \param ledRing1              the LedLightPattern for ring 1 in the outer shell
 * \param ledRing2              the LedLightPattern for ring 2 in the outer shell
 * \param ledRing3              the LedLightPattern for ring 3 in the outer shell
 * \param ledRing4              the LedLightPattern for ring 4 (top) in the outer shell
 * \param buttonLedHot          the LedLightPattern for hot button LED
 * \param buttonLedCold         the LedLightPattern for cold button LED
 * \param buttonLedClose        the LedLightPattern for close button LED
 * \param chamberLedHotEnable   whether or not to enable the lights in the hot chamber
 * \param chamberLedColdEnable  whether or not to enable the lights in the cold chamber
 * \param hotFanEnable          whether or not to enable the fan in the hot chamber
 * \param coldFanEnable         whether or not to enable the fan in the cold chamber
 * \param peltierEnable         is the Peltier element enabled
 * \param bellowState           the bellow state to be set
 */
void hardware_out(LedLightPattern ledRing0, LedLightPattern ledRing1, LedLightPattern ledRing2,
    LedLightPattern ledRing3, LedLightPattern ledRing4, LedLightPattern buttonLedHot, LedLightPattern buttonLedCold,
    LedLightPattern buttonLedClose, bool chamberLedHotEnable, bool chamberLedColdEnable, bool hotFanEnable, bool coldFanEnable,
    bool peltierEnable, BellowState bellowState);

#endif /* HARDWARE_OUT_H */
