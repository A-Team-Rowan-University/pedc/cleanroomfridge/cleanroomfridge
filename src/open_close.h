/** \file */

#ifndef OPEN_CLOSE_H
#define OPEN_CLOSE_H

#include <stdbool.h>
#include "types.h"

/// short the output signals from the open/close module
typedef struct OpenCloseOutput
{
    BellowState bellowState;        ///< the state of the bellow
    ActualPosition actualPosition;  ///< the actual position of the chamber
    OpenCloseError openCloseError;  ///< what error has been detected
} OpenCloseOutput;

/**
 * \param desiredPosition       the desired position of the chamber
 * \param positionSensorTop     true if the top position sensor is pressed
 * \param positionSensorMiddle  true if the middle position sensor is pressed
 * \param positionSensorBottom  true if the bottom position sensor is pressed
 * \param systemTime            the time that has elapsed since system startup
 * \return the output signals from the open/close module based on the input signals and the internal state
 */
OpenCloseOutput open_close(DesiredPosition desiredPosition, bool positionSensorTop, bool positionSensorMiddle, bool positionSensorBottom,
    Time systemTime);

#endif /* OPEN_CLOSE_H */
