/**
 * \file
 * hardware input module
 */

#ifndef HARDWARE_IN_H
#define HARDWARE_IN_H

#include <stdint.h>
#include <stdbool.h>
#include "types.h"

/// short the output signals from the hardware module
typedef struct HardwareInOutput
{
    bool isHotButtonPressed;        ///< true if hot button is pressed
    bool isColdButtonPressed;       ///< true if cold button is pressed
    bool isCloseButtonPressed;      ///< true if close button is pressed

    bool positionSensorTop;         ///< true if the top position sensor is pressed
    bool positionSensorMiddle;      ///< true if the middle position sensor is pressed
    bool positionSensorBottom;      ///< true if the bottom position sensor is pressed

    Temperature tempHotPeltier;     ///< the temp of hot side of peltier
    Temperature tempColdPeltier;    ///< the temp of the cold side of the peltier
    Temperature tempColdAir;        ///< the temp of the internal cold chamber
    Temperature tempHotAir;         ///< the temp of the internal hot chamber

    bool fanErrorCold;              ///< true if the cold fan dies
    bool fanErrorHot;               ///< true if the cold fan dies

    Time systemTime;                ///< the time in milliseconds that have lapsed since startup
} HardwareInOutput;

/**
 * polls the hardware and returns the sensor data
 * \return the output signals from the hardware module based on the input signals and the internal state
 */
HardwareInOutput hardware_in();

#endif /* HARDWARE_IN_H */
