/** \file */
#include <stdbool.h>
#include <stdio.h>
#include "hardware_in.h"
#include "hardware_out.h"
#include "lighting.h"
#include "temperature_control.h"
#include "user_input.h"
#include "open_close.h"

/**
 * main
 */
int main(void)
{
    HardwareInOutput hwIn;
    UserInputOutput uiOut;
    OpenCloseOutput ocOut;
    TemperatureControlOutput tempOut;
    LightingOutput lightOut;

    while(true)
        {
            hwIn = hardware_in();
            uiOut = user_input(hwIn.isHotButtonPressed, hwIn.isColdButtonPressed, hwIn.isCloseButtonPressed, ocOut.actualPosition, ocOut.openCloseError,
                    hwIn.systemTime);
            ocOut = open_close(uiOut.desiredPosition, hwIn.positionSensorTop, hwIn.positionSensorMiddle, hwIn.positionSensorBottom,
                    hwIn.systemTime);
            tempOut = temperature_control(ocOut.actualPosition, hwIn.tempHotPeltier, hwIn.tempColdPeltier, hwIn.tempHotAir,
                    hwIn.tempColdAir, hwIn.fanErrorHot, hwIn.fanErrorCold);
            lightOut = lighting(tempOut.hotPeltierState, tempOut.coldPeltierState, tempOut.hotAirState, tempOut.coldAirState,
                    hwIn.fanErrorHot, hwIn.fanErrorCold, ocOut.openCloseError, ocOut.actualPosition, uiOut.desiredPosition, hwIn.systemTime);
            hardware_out(lightOut.ledRing0, lightOut.ledRing1, lightOut.ledRing2, lightOut.ledRing3, lightOut.ledRing4, lightOut.buttonLedHot,
                lightOut.buttonLedCold, lightOut.buttonLedClose, lightOut.chamberLedHotEnable, lightOut.chamberLedColdEnable, tempOut.hotFanEnable,
                tempOut.coldFanEnable, tempOut.peltierEnable, ocOut.bellowState);
        }

    return 0;
}
