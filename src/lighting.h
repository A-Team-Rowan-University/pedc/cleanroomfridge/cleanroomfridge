/** \file */

#ifndef LIGHTING_H
#define LIGHTING_H

#include <stdint.h>
#include <stdbool.h>
#include "types.h"

/// main output state of the lighting module
typedef struct LightingOutput
{
    LedLightPattern ledRing0;       ///< the LedLightPattern for ring 0 (bottom) in the outer shell
    LedLightPattern ledRing1;       ///< the LedLightPattern for ring 1 in the outer shell
    LedLightPattern ledRing2;       ///< the LedLightPattern for ring 2 in the outer shell
    LedLightPattern ledRing3;       ///< the LedLightPattern for ring 3 in the outer shell
    LedLightPattern ledRing4;       ///< the LedLightPattern for ring 4 (top) in the outer shell
    LedLightPattern buttonLedHot;   ///< hot button pressed
    LedLightPattern buttonLedCold;  ///< cold button pressed
    LedLightPattern buttonLedClose; ///< close button pressed
    bool chamberLedHotEnable;       ///< whether or not the hot chamber leds are on
    bool chamberLedColdEnable;      ///< whether or not the hot chamber leds are on
} LightingOutput;

/**
 *  the main function for the lighting module
 * \param hotPeltierState           ///< the state of the hot side of the peltier
 * \param coldPeltierState          ///< the state of the cold side of the peltier
 * \param hotAirState               ///< the state of the air in the hot chamber
 * \param coldAirState              ///< the state of the air in the cold chamber
 * \param fanErrorHot               ///< whether or not the hot chamber fan is broken
 * \param fanErrorCold              ///< whether or not the cold chamber fan is broken
 * \param openCloseError            ///< error signal from the open/close module
 * \param actualPosition            ///< the current actual position of the chamber
 * \param desiredPosition           ///< the position the chamber is trying to go
 * \param systemTime                ///< the time that has elapsed since system startup
 * \return the output signals from the lighting module
 */
LightingOutput lighting(HotPeltierState hotPeltierState, ColdPeltierState coldPeltierState,
    HotAirState hotAirState, ColdAirState coldAirState, bool fanErrorHot, bool fanErrorCold, OpenCloseError openCloseError,
    ActualPosition actualPosition, DesiredPosition desiredPosition, Time systemTime);

#endif /* LIGHTING_H */
