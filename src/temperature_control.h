/** \file */

#ifndef TEMPERATURE_CONTROL_H
#define TEMPERATURE_CONTROL_H

#include <stdbool.h>
#include <stdint.h>
#include "types.h"

/// the high set point for the cold chamber
#define TEMPERATURE_SETPOINT_COLD_HI (Temperature) 10;

/// the low set point for the cold chamber
#define TEMPERATURE_SETPOINT_COLD_LO (Temperature) 2;

/// the main output state of the temperature_control module
typedef struct TemperatureControlOutput
{
    bool peltierEnable;                ///< the state of the Peltier Element
    bool hotFanEnable;                 ///< the state of the fan in the hot chamber
    bool coldFanEnable;                ///< the state of the fan in the cold chamber
    HotPeltierState hotPeltierState;   ///< the state of the hot side of the Peltier
    ColdPeltierState coldPeltierState; ///< the state of the cold side of the Peltier
    HotAirState hotAirState;           ///< the state of the air in the hot chamber
    ColdAirState coldAirState;         ///< the state of the air in the cold chamber
} TemperatureControlOutput;

/*
 * main function for the temperature control module
 * \param actualPosition    the actual position of the chamber, set from the open/close system
 * \param tempHotPeltier    the temperature of the hot side of the peltier element
 * \param tempColdPeltier   the temperature of the cold side of the peltier element
 * \param tempHotAir        the temperature of the hot chamber's air
 * \param tempColdAir       the temperature of the cold chamber's air
 * \param fanErrorHot       true if the hot an is broken
 * \param fanErrorCold      true if the cold an is broken
 * \return                  the temperature control output signals
 */
TemperatureControlOutput temperature_control(ActualPosition actualPosition, Temperature tempHotPeltier, Temperature tempColdPeltier,
    Temperature tempHotAir, Temperature tempColdAir, bool fanErrorHot, bool fanErrorCold);

#endif /* TEMPERATURE_CONTROL_H */
