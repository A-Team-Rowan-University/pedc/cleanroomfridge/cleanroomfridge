#include <stdlib.h>
#include <check.h>
#include "../src/hardware_out.h"

START_TEST(check_hardware_out)
{
    ck_assert_int_eq(1, 1);
}
END_TEST

Suite * hardware_out_suite(void)
{
    Suite *s;
    TCase *tc_core;
    s = suite_create("Hardware Out");
    // Core test case
    tc_core = tcase_create("Core");
    tcase_add_test(tc_core, check_hardware_out);
    suite_add_tcase(s, tc_core);
    return s;
}

int main(void)
{
    int number_failed;
    Suite *s;
    SRunner *sr;
    s = hardware_out_suite();
    sr = srunner_create(s);
    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
