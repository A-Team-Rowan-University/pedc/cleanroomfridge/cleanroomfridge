# Clean Room Environmental Chamber Embedded Software

## Wiki
See our GitLab [Wiki](https://gitlab.com/A-Team-Rowan-University/pedc/cleanroomfridge/cleanroomfridge/-/wikis/home)

## C Library Documentation
View our [Documentaion](https://a-team-rowan-university.gitlab.io/pedc/cleanroomfridge/cleanroomfridge/)

## Building
```
autoreconf -i
automake
mkdir build
cd build
../configure
make
```

## Testing
```
make check
```
